package com.rekent.suerityDemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Spring Security Config
 * @author chenhao.zhang
 *
 */
@Configuration
@EnableWebSecurity
public class SuerityConfig  extends WebSecurityConfigurerAdapter{

	@Autowired
	SuccessHandler successHandler;
	
	@Autowired
	UserAuthProvider userProvider;
	
	@Autowired
	UserAfterProvider afterProvider;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
		//.antMatchers("/").permitAll()
		.antMatchers("/index").hasAnyAuthority("ROLE_USER","ROLE_ADMIN")
		.antMatchers("/oss").hasAuthority("ROLE_ADMIN")
		.antMatchers(HttpMethod.GET, "/login").permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/login")
		.permitAll().successHandler(successHandler)
		.and()
		.logout()
		.logoutSuccessUrl("/")
		.permitAll();
	}

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication().passwordEncoder(new BCryptPasswordEncoder())
//				.withUser("root").password(new BCryptPasswordEncoder().encode("root")).roles("USER","ADMIN").and()
//				.withUser("normal").password(new BCryptPasswordEncoder().encode("normal")).roles("USER");
		auth.authenticationProvider(userProvider);
		auth.authenticationProvider(afterProvider);
		
	}
	
	
}
