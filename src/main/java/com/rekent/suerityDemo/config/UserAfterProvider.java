package com.rekent.suerityDemo.config;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class UserAfterProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		System.out.println("This is UserAfterProvider");

		System.out.println("starting authenticate ... ...");
		System.out.println("Credentials:" + authentication.getCredentials());
		System.out.println("Name:" + authentication.getName());
		System.out.println("Class:" + authentication.getClass());
		System.out.println("Details:" + authentication.getDetails());
		System.out.println("Principal:" + authentication.getPrincipal());
		UsernamePasswordAuthenticationToken auth=new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials());
		return auth;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		System.out.println("This is UserAfterProvider");
		System.out.println("starting supports");
		System.out.println(authentication.getClass());
		return true;
	}

}
