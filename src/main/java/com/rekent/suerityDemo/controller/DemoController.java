package com.rekent.suerityDemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DemoController {
	
	@RequestMapping(value="/index")
	public String index() {
		return "index";
	}
	
	@RequestMapping(value="/oss")
	public String oss() {
		return "oss";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String toLogin() {
		return "Login";
				
	}
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String toIndex() {
		return "index";
	}


}
